extends TextureRect

class_name Tile

var is_head : bool = false
var player : int = -1
var is_food : bool = false
var is_active : bool = false

var tween : Tween
var tile_x : int
var tile_y : int
var tile_size : int

func _ready():
	add_to_group("tiles")
	tween = Tween.new()
	add_child(tween)

func teleport_to(x, y):
	rect_position = Vector2(x * tile_size, y * tile_size)
	tile_x = x
	tile_y = y

func move_to(x, y):
	tween.interpolate_property(self, ":rect_position", rect_position, Vector2(x * tile_size, y * tile_size), 0.2,Tween.TRANS_CUBIC)
	tile_x = x
	tile_y = y
	tween.start()
