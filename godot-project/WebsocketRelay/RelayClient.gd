extends Node

class_name RelayClient

export(String) var websocket_url = "godot-server.captain.perons.com.br"
export(int) var port = 9080

var _match
var _id = 0
var _player_number = 0

var _client = WebSocketClient.new()
onready var _initialised = false

var uri : String

signal on_message(message)

func send_data(message : Message):
	_client.get_peer(1).put_packet(message.get_raw())

func connect_to_server():
	uri = "ws://" + websocket_url + ":" + str(port)
	
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")

	_client.connect("data_received", self, "_on_data")

	var err = _client.connect_to_url(uri)
	if err != OK:
		set_process(false)

func disconnect_from_server():
	_client.disconnect_from_host()

func _closed(was_clean = false):
	print("Closed, clean: ", was_clean)
	set_process(false)

func _connected(proto = ""):
	print("Connected to server!")

func _on_data():
	var data = _client.get_peer(1).get_packet()
	
	var message = Message.new()
	message.from_raw(data)
	
	if (message.server_login):
		_id = message.content
		_initialised = true
		print("Logged in with id ", _id)
	if (message.match_start):
		_match = message.content as Array
		_player_number = _match.find(_id)
		print("Match started as player ", _player_number)
	else:
		print("On message: ", message.content)
	emit_signal("on_message", message)

func _process(delta):
	_client.poll()

func _notification(what):
	if what == NOTIFICATION_PREDELETE:
		_client.disconnect_from_host()
